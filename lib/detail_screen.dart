import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'connection/global_func.dart';

class Detail_Screen extends StatefulWidget {
  var pokeData = {};
  var index = 0;

  Detail_Screen(this.pokeData, this.index);

  @override
  Detail_Screen_State createState() => Detail_Screen_State();
}

class Detail_Screen_State extends State<Detail_Screen> {
  late Size deviceSize;

  int indexChoosed = 0;

  var pokeEvolution = {};

  GetPokemonEvolution() async {
    var tempResult = await ConnectionToAPI(
        "https://pokeapi.co/api/v2/evolution-chain/" +
            widget.pokeData['id'].toString());

    setState(() {
      pokeEvolution = tempResult;
    });
  }

  PageController _controller = PageController(
    initialPage: 0,
  );

  OnMenuClick(int index) {
    setState(() {
      indexChoosed = index;
      _controller.jumpToPage(index);
    });
  }

  @override
  void initState() {
    GetPokemonEvolution();
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    typesBuilder() {
      List<Widget> tempWidget = [];


      var tempList = widget.pokeData['types'];

      for (int i = 0; i < tempList.length; i++) {
        tempWidget.add(Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: Colors.white,
          ),
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 8, right: 8),
          margin: EdgeInsets.all(5),
          child: Text(
            tempList[i]['type']['name'].toString().toUpperCase(),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ));
      }

      return tempWidget;
    }

    menu_0_builder() {
      getAbilities() {
        var tempString = "";

        List tempAbilities = widget.pokeData['abilities'];

        for (int i = 0; i < tempAbilities.length; i++) {
          tempString += tempAbilities[i]['ability']['name'] + ", ";
        }

        tempString = tempString
            .replaceRange(tempString.length - 2, tempString.length, "")
            .toString();
        return tempString;
      }

      return SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Flexible(
                        flex: 4,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            "Species",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        )),
                    Flexible(
                        flex: 6,
                        child: Container(
                          width: double.infinity,
                          child: Text(widget.pokeData['species']['name'],
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17)),
                        ))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Row(
                  children: [
                    Flexible(
                        flex: 4,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            "Height",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        )),
                    Flexible(
                        flex: 6,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                              (widget.pokeData['height'] / 10).toString() +
                                  " m",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17)),
                        ))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Row(
                  children: [
                    Flexible(
                        flex: 4,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            "Weight",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        )),
                    Flexible(
                        flex: 6,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                              (widget.pokeData['weight'] / 10).toString() +
                                  " Kg",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17)),
                        ))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Row(
                  children: [
                    Flexible(
                        flex: 4,
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            "Abilities",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        )),
                    Flexible(
                        flex: 6,
                        child: Container(
                          width: double.infinity,
                          child: Text(getAbilities(),
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17)),
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }

    menu_1_builder() {
      List tempStats = widget.pokeData['stats'];

      return ListView.builder(
          itemBuilder: (context, index) {
            var tempName =
                tempStats[index]['stat']['name'].replaceAll("-", " ");

            tempName = tempName
                .toString()
                .replaceRange(0, 1, tempName[0].toString().toUpperCase());

            return Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  Flexible(
                      flex: 3,
                      child: Container(
                        width: double.infinity,
                        child: Text(tempName,
                            style: TextStyle(color: Colors.grey, fontSize: 17)),
                      )),
                  Flexible(
                      flex: 2,
                      child: Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        child: Text(tempStats[index]['base_stat'].toString(),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontWeight: FontWeight.bold)),
                      )),
                  Flexible(
                      flex: 6,
                      child: Container(
                        width: double.infinity,
                        child: LinearProgressIndicator(
                            color: Colors.blue,
                            backgroundColor: Colors.grey.shade100,
                            value: tempStats[index]['base_stat'] / 100),
                      ))
                ],
              ),
            );
          },
          itemCount: tempStats.length);
    }

    menu_2_builder() {
      List<Widget> tempList = [];

      // if (pokeEvolution.isNotEmpty) {
      //   var tempEvData = pokeEvolution['chain'];
      //
      //   getEvolution(var data) {
      //     tempList.add(Text(data[0]['species']['name']));
      //
      //     if (data[0]['evolves_to'].length != 0) {
      //       getEvolution(data[0]['evolves_to']);
      //     }
      //   }
      //
      //   getEvolution(tempEvData['evolves_to']);
      // }

      return Container(
        child: Column(
          children: tempList,
        ),
      );
    }

    menu_3_builder() {
      List tempStats = widget.pokeData['moves'];

      return ListView.builder(
          itemBuilder: (context, index) {
            var tempName =
                tempStats[index]['move']['name'].replaceAll("-", " ");

            tempName = tempName
                .toString()
                .replaceRange(0, 1, tempName[0].toString().toUpperCase());

            return Container(
              margin: EdgeInsets.only(top: 10),
              child: Container(
                width: double.infinity,
                child: Text(tempName,
                    style: TextStyle(color: Colors.grey, fontSize: 17)),
              ),
            );
          },
          itemCount: tempStats.length);
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      backgroundColor: Colors.red,
      body: SafeArea(
          child: Stack(children: [  Positioned( top: 0,right: -50,
              child: Container(
                constraints: BoxConstraints(maxWidth: 500   ,maxHeight: 500
                ),
                child: Opacity(child: Image.asset("assets/pokeball.png",color: Colors.white ),opacity: .2,)
              )),Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.pokeData['name'].toString().toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: Colors.white),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15, left: 15, right: 15),
                child: Row(
                  children: typesBuilder(),
                ),
              ),
              Container(
                  alignment: Alignment.center,
                  child: Hero(
                    child: Image.network(
                      widget.pokeData['sprites']['other']['home']['front_default'],
                      scale: 2.5,
                    ),
                    tag: "pokepic" + widget.index.toString(),
                  )),
              Flexible(
                child: Container(
                  height: deviceSize.height,
                  width: double.infinity,
                  padding: EdgeInsets.all(25),
                  margin: EdgeInsets.only(top: deviceSize.width * .1),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Flexible(
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: InkWell(
                                    onTap: () {
                                      OnMenuClick(0);
                                    },
                                    child: Container(
                                      child: Text(
                                        "About",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: indexChoosed == 0
                                                ? Colors.black
                                                : Colors.grey,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                )),
                            Flexible(
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: InkWell(
                                    onTap: () {
                                      OnMenuClick(1);
                                    },
                                    child: Container(
                                      child: Text("Base Stats",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: indexChoosed == 1
                                                  ? Colors.black
                                                  : Colors.grey,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                )),
                            // Flexible(
                            //     child: Container(
                            //   width: double.infinity,
                            //   alignment: Alignment.center,
                            //   child: InkWell(
                            //     onTap: () {
                            //       OnMenuClick(2);
                            //     },
                            //     child: Container(
                            //       child: Text("Evolution",
                            //           style: TextStyle(
                            //               fontSize: 18,
                            //               color: indexChoosed == 2
                            //                   ? Colors.black
                            //                   : Colors.grey,
                            //               fontWeight: FontWeight.bold)),
                            //     ),
                            //   ),
                            // )),
                            Flexible(
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: InkWell(
                                    onTap: () {
                                      OnMenuClick(2);
                                    },
                                    child: Container(
                                      child: Text("Moves",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: indexChoosed == 2
                                                  ? Colors.black
                                                  : Colors.grey,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ),
                      Flexible(
                          child: Container(
                              margin: EdgeInsets.only(top: 25),
                              height: double.infinity,
                              width: double.infinity,
                              child: PageView(
                                onPageChanged: (value) {
                                  setState(() {
                                    indexChoosed = value;
                                  });
                                },
                                controller: _controller,
                                children: [
                                  menu_0_builder(),
                                  menu_1_builder(),
                                  // menu_2_builder(),
                                  menu_3_builder()
                                ],
                              )))
                    ],
                  ),
                ),
              )
            ],
          )],)),
    );
  }
}
