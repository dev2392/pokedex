import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/detail_screen.dart';

import 'connection/global_func.dart';

class Mainmenu_Screen extends StatefulWidget {
  @override
  Mainmenu_Screen_State createState() => Mainmenu_Screen_State();
}

class Mainmenu_Screen_State extends State<Mainmenu_Screen> {
  late Size deviceSize;

  var nextUrl = '';
  List pokemonList = [];

  final ScrollController _controller = ScrollController();

  GetPokemonList(var url) async {
    var tempResult = await ConnectionToAPI(url);

    print("ger data ");
    print(url);

    setState(() {
      pokemonList.addAll(tempResult['results']);
      nextUrl = tempResult['next'];
    });
  }

  @override
  void initState() {
    GetPokemonList("https://pokeapi.co/api/v2/pokemon/");

    _controller.addListener(() {
      if (_controller.position.atEdge) {
        bool isTop = _controller.position.pixels == 0;
        if (!isTop) {
          print('At the bottom');

          if (nextUrl.isNotEmpty) {
            GetPokemonList(nextUrl);
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;

    listItemBuilder(var data, var index) {
      return Builder(
        builder: (context) {
          var tempData = {};

          return StatefulBuilder(
            builder: (context, setState) {
              GetPokemonDetail() async {
                var tempResult = await ConnectionToAPI(data['url']);

                setState(() {
                  tempData = tempResult;
                });
              }

              if (tempData.isEmpty) {
                GetPokemonDetail();
              }

              typesBuilder() {
                List<Widget> tempWidget = [];

                var tempList = tempData['types'];

                for (int i = 0; i < tempList.length; i++) {
                  tempWidget.add(Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(left: 8, right: 8),
                    margin: EdgeInsets.all(5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      tempList[i]['type']['name'].toString().toUpperCase(),
                      style: TextStyle(),
                    ),
                  ));
                }

                return tempWidget;
              }

              return Card(
                  child: Container(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Detail_Screen(tempData, index),
                        ));
                  },
                  child: Container(
                      child: tempData.isEmpty
                          ? Container()
                          : Stack(
                              children: [
                               Container(
                                    child: Column(
                                      children: [
                                        Container(margin:EdgeInsets.all(10),alignment:Alignment.centerLeft,child: Text(
                                          tempData['name']
                                              .toString()
                                              .toUpperCase(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),),
                                        Column(
                                          children: typesBuilder(),
                                        )
                                      ],
                                    ),
                                  ),

                                Positioned(right: 10  ,bottom: 10,
                                    child: Container(
                                      constraints: BoxConstraints(maxWidth: 50,maxHeight: 50
                                      ),
                                      child: Opacity(child: Image.asset("assets/pokeball.png"),opacity: .1,),
                                    )),

                                Positioned(
                                    bottom: 10,
                                    right: 10,
                                    child: tempData.isNotEmpty
                                        ? Hero(
                                            tag: "pokepic" + index.toString(),
                                            child: Image.network(
                                              tempData['sprites']['other']
                                                  ['home']['front_default'],
                                              fit: BoxFit.fill,
                                              height: 80,
                                              width: 80,
                                            ))
                                        : Container(
                                            color: Colors.red,
                                          ))
                              ],
                            ),
                      decoration: BoxDecoration()),
                ),
              ));
            },
          );
        },
      );
    }

    return Scaffold(

      body: SafeArea(
        child: Stack(
          children: [
            Positioned(right: -100  ,top: 0,
                child: Container(
                  constraints: BoxConstraints(maxWidth: 250,maxHeight: 250
                  ),
              child: Opacity(child: Image.asset("assets/pokeball.png",color: Colors.red,),opacity: .3,),
            )),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15,top: deviceSize.width*.3),
                  width: double.infinity,
                  child: Text(
                    "Pokedex",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
                Flexible(
                    child: Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                  height: double.infinity,
                  child: GridView.builder(
                      controller: _controller,
                      itemBuilder: (context, index) {
                        return listItemBuilder(pokemonList[index], index);
                      },
                      itemCount: pokemonList.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 1.3, crossAxisCount: 2)),
                ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
